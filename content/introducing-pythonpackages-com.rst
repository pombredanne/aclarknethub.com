Introducing pythonpackages.com
##############################
:date: 2011-11-29 02:24
:tags: Python

The website for Python egoists™
-------------------------------

I have this overwhelming desire to know how many times my favorite
Python packages have been downloaded. Don't you? If so, look no further
than `pythonpackages.com`_:

`|image0|`_\ Count downloads
----------------------------

Where you can enter a package like `Django`_ and find out:

`|image1|`_\ Count favorites
----------------------------

Or click on **Count favorites** to reveal how many times it has been
favorited:

`|image2|`_

Find trash
----------

Or even poke some good natured fun at it, for its `failure to provide
adequate package metadata`_:

`|image3|`_
-----------

Discuss packages
----------------

You can even *discuss*\ packages on `pythonpackages.com`_:

`|image4|`_

Recent activity
---------------

But wait there is more! You can also browse the `latest changelog
activity`_ from PyPi right on the site:

`|image5|`_

Package info
------------

Lastly, if you get bored counting package downloads and favorites you
can look at the *actual* `package metadata`_.

`|image6|`_

Conclusion
----------

Sound interesting? Or utterly silly, perhaps? I think
`pythonpackages.com`_ is the best of both. It started off as a front end
to the command line utility `vanity`_, but has taken on a life of it's
own.

After a small run on Python reddit and Hacker News, I'm looking forward
to seeing what Python Planet folks think. So please check out
`pythonpackages.com`_ and let me know. You can leave comments about the
site here:

-  `http://pythonpackages.com/about`_

Or open a new ticket here:

-  `https://bitbucket.org/pythonpackages/pythonpackages.com/issues/new`_

.. _pythonpackages.com: http://pythonpackages.com
.. _|image7|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-03-48-am.png
.. _Django: http://pythonpackages.com/info/django
.. _|image8|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-11-25-am.png
.. _|image9|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-12-04-am.png
.. _failure to provide adequate package metadata: http://pythonpackages.com/trash/django
.. _|image10|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-12-24-am.png
.. _|image11|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-39-37-am.png
.. _latest changelog activity: http://pythonpackages.com/pypi
.. _|image12|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-46-38-am.png
.. _package metadata: http://pythonpackages.com/info/django
.. _|image13|: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-51-59-am.png
.. _vanity: http://pythonpackages.com/info/vanity
.. _`http://pythonpackages.com/about`: http://pythonpackages.com/about
.. _`https://bitbucket.org/pythonpackages/pythonpackages.com/issues/new`: https://bitbucket.org/pythonpackages/pythonpackages.com/issues/new

.. |image0| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-03-48-am.png
.. |image1| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-11-25-am.png
.. |image2| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-12-04-am.png
.. |image3| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-12-24-am.png
.. |image4| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-39-37-am.png
.. |image5| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-46-38-am.png
.. |image6| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-51-59-am.png
.. |image7| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-03-48-am.png
.. |image8| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-11-25-am.png
.. |image9| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-12-04-am.png
.. |image10| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-12-24-am.png
.. |image11| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-39-37-am.png
.. |image12| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-46-38-am.png
.. |image13| image:: http://aclark4life.files.wordpress.com/2011/11/screen-shot-2011-11-29-at-1-51-59-am.png

ARTICLE_SAVE_AS = '/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html'
ARTICLE_URL = '/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
AUTHOR = u'aclark'
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
DEFAULT_CATEGORY = 'Blog'
DEFAULT_LANG = 'en'
DEFAULT_PAGINATION = 10
DELETE_OUTPUT_DIRECTORY = True
DISQUS_SITENAME = 'aclark-blog'
GITHUB_URL = 'https://github.com/ACLARKNET/aclarknet.github.com'
GOOGLE_ANALYTICS = 'UA-34988446-1'
SITENAME = u'Alex Clark'
SITEURL = 'http://blog.aclark.net'
SOCIAL = (
    ('aclark4life', 'http://twitter.com/aclark4life'),
    ('ACLARKNET', 'http://twitter.com/ACLARKNET'),
    ('GitHub', 'http://github.com/aclark4life'),
    ('Gittip', 'https://www.gittip.com/aclark4life'),
    ('PythonPackages', 'https://pythonpackages.com/user/aclark4life'),
    ('atom feed (Django)', 'http://blog.aclark.net/feeds/Django.atom.xml'),
    ('atom feed (Mozilla)', 'http://blog.aclark.net/feeds/Mozilla.atom.xml'),
    ('atom feed (Plone)', 'http://blog.aclark.net/feeds/Plone.atom.xml'),
    ('atom feed (Python)', 'http://blog.aclark.net/feeds/Python.atom.xml'),
)
TAG_FEED_ATOM = 'feeds/%s.atom.xml'
TAG_FEED_RSS = None
THEME = 'notmyidea_solarized'
TWITTER_USERNAME = 'aclark4life'

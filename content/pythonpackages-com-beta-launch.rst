pythonpackages.com beta launch
##############################
:date: 2012-07-04 05:19
:tags: Mozilla, Plone, Python

After 9 months of development, pythonpackages.com has
grand-ceremoniously flipped the switch from alpha to beta. Here is an
overview of the beta release features.

**Killer GitHub integration**
-----------------------------

The workflow you may now enjoy is:

-  Login with your GitHub account
-  Select a repository that contains a Python package

One-click release
-----------------

More sweet workflow:

-  Click a button to release to a test index, PyPI

Create packages through-the-web (with PasteScript)
--------------------------------------------------

There's a lot of room for growth, but this feature is exciting even in
its simplest form. 

Dashboard o releases
--------------------

pythonpackages.com keeps track of your releases, using the same
technology that powers the alpha release "package featuring" feature.
`|image6|`_

GitHub Organization support
---------------------------

But wait, there's more. Paid plans (which have not been deployed yet)
get access to GitHub organizations, whose repositories can then be
"slotted" like normal. `|image7|`_ `|image8|`_ `|image9|`_ `|image10|`_
`|image11|`_ If you'd like to get started using the site, check out the
`crash course`_.

.. _|image12|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-15-58-pm.png
.. _|image13|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-07-38-pm.png
.. _|image14|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-24-38-pm1.png
.. _|image15|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-27-50-pm.png
.. _|image16|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-31-41-pm.png
.. _|image17|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-36-23-pm.png
.. _|image18|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-50-23-pm.png
.. _|image19|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-52-47-am.png
.. _|image20|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-54-50-am.png
.. _|image21|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-15-am.png
.. _|image22|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-26-am.png
.. _|image23|: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-46-am.png
.. _crash course: http://docs.pythonpackages.com/en/latest/crashcourse.html

.. |image0| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-15-58-pm.png?w=300
.. |image1| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-07-38-pm.png?w=300
.. |image2| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-24-38-pm1.png?w=300
.. |image3| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-27-50-pm.png?w=300
.. |image4| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-31-41-pm.png?w=300
.. |image5| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-36-23-pm.png?w=300
.. |image6| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-50-23-pm.png?w=300
.. |image7| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-52-47-am.png?w=300
.. |image8| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-54-50-am.png?w=300
.. |image9| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-15-am.png?w=300
.. |image10| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-26-am.png?w=300
.. |image11| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-46-am.png?w=300
.. |image12| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-15-58-pm.png?w=300
.. |image13| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-07-38-pm.png?w=300
.. |image14| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-24-38-pm1.png?w=300
.. |image15| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-27-50-pm.png?w=300
.. |image16| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-31-41-pm.png?w=300
.. |image17| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-36-23-pm.png?w=300
.. |image18| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-03-at-11-50-23-pm.png?w=300
.. |image19| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-52-47-am.png?w=300
.. |image20| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-54-50-am.png?w=300
.. |image21| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-15-am.png?w=300
.. |image22| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-26-am.png?w=300
.. |image23| image:: http://aclark4life.files.wordpress.com/2012/07/screen-shot-2012-07-04-at-12-56-46-am.png?w=300
